function disableSubmitButton(){
    if ($('#id_testimonial').val().length > 0) {
        $("input[type=submit]").prop("disabled", false);
    }
    else {
        $("input[type=submit]").prop("disabled", true);
    }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    return(/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

var csrftoken = getCookie('csrftoken');

$(document).ready(function () {
    disableSubmitButton();
    $('#id_testimonial').on('change', function () {
        disableSubmitButton();
    });
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });

    $('#testimonial').on('submit', function (e) {
        e.preventDefault();
        console.log("Form is submitted!");
        var testimonial = $('#id_testimonial');
        var data = {
            'testimonial': testimonial.val(),
        };
        console.log(data);
        $.ajax({
            method: 'POST',
            url: '/comment/',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.success) {
                    console.log("Form is success!");
                    alert(data.message);
                    $('#result').prepend(
                        '<div class="card bg-light mb-3 container">' +
                        '<div class="card-header">' + new Date().toUTCString() + '</div>' +
                        '<div class="card-body">' +
                        '<h4 class="card-title" style="font-weight: bold">Username</h4>' +
                        '<p class="card-text">' + data['testimonial'] + '</p>' +
                        '</div>' +
                        '</div>'
                    );
                }
                else {
                    console.log("Form is not success!");
                    alert(data.message);
                }
            }
        }).then(function (data) {
            if (data.success) {
                testimonial.val("");
            }
        })
    });
});
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.core.exceptions import ValidationError
from .models import modelsDonatur
from .forms import formPage
from .views import *

class RegisterUnitTest(TestCase):

    def test_register_url_exists(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_create_object_model(self):
        modelsDonatur.objects.create(username='Ringgi', birthday='1999-02-25', email='ringgicahyo@gmail.com', password='remember itcover')
        counting_object_status = modelsDonatur.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_register_post_success_and_render_the_result(self):
        username = 'test'
        birthday = '1999-02-25'
        email = 'test@email.com'
        password = 'testpassword'
        response_post = Client().post('/register/', {'username': username, 'birthday': birthday, 'email': email, 'password': password})
        self.assertTrue(modelsDonatur.objects.filter(email=email).exists())

    def test_register_post_error_and_render_the_result(self):
        username = 'test'
        birthday = '1999-13-25'
        email = 'test@email.com'
        password = 'testpassword'
        response_post = Client().post('/register/', {'username': username, 'birthday': birthday, 'email': email, 'password': password})
        html_response = response_post.content.decode('utf8')
        self.assertIn(username, html_response)
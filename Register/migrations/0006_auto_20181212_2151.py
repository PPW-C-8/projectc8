# Generated by Django 2.1.1 on 2018-12-12 14:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Register', '0005_auto_20181212_1618'),
    ]

    operations = [
        migrations.AlterField(
            model_name='modelsdonatur',
            name='email',
            field=models.EmailField(max_length=50, unique=True),
        ),
    ]

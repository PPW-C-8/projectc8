from django import forms
from .models import modelsDonatur
from django.core.exceptions import ValidationError

class formPage(forms.Form):
    username = forms.CharField(label='Username', max_length=100, required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter a Username'}))
    birthday = forms.DateField(label="Date of Birth", required=True,
                               widget=forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}))
    email = forms.EmailField(label='Email', max_length=50, required=True,
                             widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Enter Your E-mail'}))
    password = forms.CharField(label='Password', max_length=20, required=True,
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Enter Your Password'}))

    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            modelsDonatur.objects.get(email=email)
        except modelsDonatur.DoesNotExist:
            return email
        # raise ValidationError('Email exists. Please use another email.')

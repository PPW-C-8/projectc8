from django.db import models
from django.contrib.auth.models import User

class modelsDonatur(models.Model):
    username = models.CharField(max_length=100)
    birthday = models.DateField(null=True)
    email = models.EmailField(max_length=50, unique=True)
    password = models.CharField(max_length=20)

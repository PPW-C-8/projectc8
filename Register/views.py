from django.shortcuts import render
from .models import modelsDonatur
from .forms import formPage
from django.http import HttpResponseRedirect

def register(request):
    response = {}
    donatur = formPage(request.POST)
    if request.user.is_authenticated:
        user = request.user
        if not modelsDonatur.objects.filter(email=user.email).exists():
            donatur_database = modelsDonatur.objects.create(username=user.username, email=user.email)
            response['donatur'] = {'username': donatur_database.name, 'email': donatur_database.email}
            return render(request, 'Register.html', {'form': donatur})
        return render(request, 'Register.html', {'form': donatur})

    if request.method == 'POST':
        if donatur.is_valid():
            response['username'] = request.POST['username']
            response['birthday'] = request.POST['birthday']
            response['email'] = request.POST['email']
            response['password'] = request.POST['password']
            donatur_database = modelsDonatur(username=request.POST['username'],
                                    birthday=request.POST['birthday'],
                                    email=request.POST['email'],
                                    password=request.POST['password'])
            donatur_database.save()
            return HttpResponseRedirect('/')
        else:
            return render(request, 'Register.html', {'form': donatur})
    else:
        donatur = formPage()
        return render(request, 'Register.html', {'form': donatur})




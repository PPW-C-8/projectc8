## ProjectC8 - Tugas 1 Kelompok 8 Kelas C PPW 2018
## Nama Anggota Kelompok
- Aghni Anugrah Raesa (1706025296)
- Arditio Triadi Riski (1706022110)
- Arif Teguh Wangi (1706040012)
- Ringgi Cahyo Dwiputra (1706025005)

## Status Aplikasi
[![pipeline status](https://gitlab.com/PPW-C-8/projectc8/badges/master/pipeline.svg)](https://gitlab.com/PPW-C-8/projectc8/commits/master)
[![coverage report](https://gitlab.com/PPW-C-8/projectc8/badges/master/coverage.svg)](https://gitlab.com/PPW-C-8/projectc8/commits/master)

## Link Herokuapp
Link: [https://projectc8.herokuapp.com/](https://projectc8.herokuapp.com/)

## Check List
TO DO List

UMUM
* [X] Beri proteksi terhadap URL yang perlu login (mencegah user yang belum login untuk mengakses URL tersebut; dalam hal ini Halaman Donasi).
* [X] Login dengan Google Social Auth.
* [X] Menyapa user setelah login dengan menyebutkan nama user.
* [X] Redirect ke halaman Daftar Program setelah login.
* [X] Setelah login dengan Social Auth, data user disimpan ke database.
* [X] Pastikan session terjaga untuk setiap halaman jika sudah login.

PROGRAM
* [X] Jika user sudah login, melakukan donasi hanya dengan tekan tombol "Donasi".
* [X] Jika user belum login, munculkan Modal (popup) yang meminta login dengan memunculkan tombol Social Google Auth.
* [X] Donasi yang diberikan oleh user disimpan di database.

DAFTAR DONASI
* [X] Menampilkan daftar donasi (nama program dan besaran donasinya) yang sudah didaftar oleh user tersebut (user specific).
* [ ] Daftar donasi ditampilkan dengan AJaX dan Web Service yang dapat diakses dengan melakukan GET ke suatu URI.
* [X] Menampilkan jumlah donasi yang sudah pernah dilakukan oleh suatu user di bagian atas halaman.

COMPANY PROFILE
* [X] Buat page baru dengan konten profil organisasi dan fitur testimoni pengunjung.
* [X] Testimoni bisa dilihat oleh user tanpa perlu log in.
* [X] Text area untuk memberi komentar (testimoni) hanya untuk user yang sudah log in.
* [X] Update komentar di halaman tanpa reload (AJaX).

## Pembagian Tugas
1. Arditio Triadi Riski
   - Login (secara keseluruhan, menyapa user)
2. Aghni Anugrah Raesa
   - Program (jika sudah login langsung donasi atau jika belum diminta untuk login)
3. Arif Teguh Wangi
   - Daftar Donasi (menampilkan riwayat donasi yang pernah dilakukan user)
4. Ringgi Cahyo Dwiputra
   - Company Profile (halaman About - tentang SinarPerak, fitur testimoni pengunjung)

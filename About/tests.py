from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.contrib.auth.models import User
from .views import *

class AboutUnitTest(TestCase):

    def setUp(self):
        user = User.objects.create(username="ringgicahyo")
        user.set_password('remember it')
        user.save()
        logged_in = self.client.login(username='test', password='12345')

    def test_about_url_exists(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_comment_result_url_exists(self):
        response = Client().get('/testimonial/')
        self.assertEqual(response.status_code, 200)

    def test_delete_database_url_exists(self):
        response = Client().get('/delete/')
        self.assertEqual(response.status_code, 302)

    def test_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_comment_view_func(self):
        found = resolve('/comment/')
        self.assertEqual(found.func, comment_view)

    def test_comment_result_func(self):
        found = resolve('/testimonial/')
        self.assertEqual(found.func, comment_result)

    def test_delete_database_func(self):
        found = resolve('/delete/')
        self.assertEqual(found.func, delete_database)

    def test_create_object_testimonial_model(self):
        aboutModels.objects.create(testimonial='Webnya bagus')
        counting_object_status = aboutModels.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_comment_post_success_and_give_feedback(self):
        testimonial = 'Webnya bagus'
        username = "ringgicahyo"
        response_post = self.client.post('/comment/', {'user': username, 'testimonial': testimonial})
        self.assertEqual(response_post.status_code, 302)
        self.assertFalse(aboutModels.objects.filter(testimonial=testimonial).exists())

    def test_comment_is_not_valid(self):
        testimonial = ''
        response_post = self.client.post('/comment/', {'testimonial': testimonial})
        self.assertEqual(response_post.status_code, 302)


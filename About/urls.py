from django.urls import path
from .views import *

urlpatterns = [
    path('about/', about, name='about'),
    path('comment/', comment_view, name='comment'),
    path('testimonial/', comment_result, name='testimonial'),
    path('delete/', delete_database, name='delete'),
]
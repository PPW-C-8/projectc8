from django.shortcuts import render
from .forms import aboutPage
from .models import aboutModels
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
import json

def about(request):
    response = {}
    html = 'About.html'
    response['testi'] = aboutPage()
    result = aboutModels.objects.all().order_by('date').reverse()
    response['result'] = result
    return render(request, html, response)

def comment_view(request):
    response = {}
    testi = aboutPage(request.POST or None)
    if request.method == 'POST' and request.user.is_authenticated:
        user = request.user
        if testi.is_valid():
            response['testimonial'] = request.POST['testimonial']
            testimonial_form = aboutModels.objects.create(user=user, testimonial=request.POST['testimonial'])
            response['message'] = 'Testimoni berhasil disimpan. Terima kasih.'
            response['username'] = {'user': testimonial_form.user.username, 'testimonial': testimonial_form.testimonial}
            response['success'] = True
            json_data = json.dumps(response)
            return HttpResponse(json_data, content_type='application/json')
        else:
            testi.clean()
            response['message'] = 'Testimoni tidak berhasil disimpan. Silahkan coba lagi.'
            response['success'] = False
            json_data = json.dumps(response)
            return HttpResponse(json_data, content_type='application/json')
    else:
        return HttpResponseRedirect('/about/')

def comment_result(request):
    result = aboutModels.objects.all().order_by('date')
    response = {'result': result}
    html = 'About.html'
    return render(request, html, response)

def delete_database(request):
    delete = aboutModels.objects.all().delete()
    response = {'delete': delete, 'testi': aboutPage}
    return HttpResponseRedirect('/about/')

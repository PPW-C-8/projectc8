from django.db import models
from django.conf import settings

class aboutModels(models.Model):
    testimonial = models.CharField(max_length=1000)
    date = models.DateTimeField(auto_now_add=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)

from django import forms

class aboutPage(forms.Form):
    testimonial = forms.CharField(label='Testimonial', widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Comment here'}))

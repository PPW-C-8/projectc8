from django.db import models

class donasi(models.Model):
    program = models.CharField(max_length=50)
    username = models.CharField(max_length=100)
    email = models.EmailField(max_length=50)
    jumlah_donasi = models.IntegerField()
    anonim = models.BooleanField(default=False)

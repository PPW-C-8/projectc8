from django.test import *
from .views import *
from .models import *
from django.urls import *

class tester(TestCase):
    def test_donasi_url_exists(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 302)

    def test_brosur_func(self):
        found = resolve('/donasi/')
        self.assertEqual(found.func, brosur)

    def test_create_object_model(self):
        donasi.objects.create(program='acara', username='Jamal', email='cobacoba@gmail.com', jumlah_donasi=10000, anonim=False)
        counting_object_status = donasi.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_donation_post_success_and_render_the_result(self):
        program = 'test1'
        name = 'test'
        email = 'test@email.com'
        jumlah_donasi = 1000000
        anonim = False
        response_post = Client().post('/donasi/', {'program': program, 'name': name, 'email': email, 'jumlah_donasi': jumlah_donasi, 'anonim': anonim})
        self.assertFalse(donasi.objects.filter(jumlah_donasi=jumlah_donasi).exists())

    def test_donasi_post_error_and_render_the_result(self):
        program = 'test1'
        name = ''
        email = 'test@email.com'
        jumlah_donasi = -20000
        anonim = False
        response_post = Client().post('/donasi/', {'program': program, 'name': name, 'email': email, 'jumlah_donasi': jumlah_donasi, 'anonim': anonim})
        html_response = response_post.content.decode('utf8')
        self.assertIn(name, html_response)

    def test_url_riwayat_exist(self):
        response = Client().get('/history/')
        self.assertEqual(response.status_code, 200)

    def test_riwayat_template(self):
        response = Client().get('/history/')
        self.assertTemplateUsed(response, 'riwayat.html')

    def test_using_riwayat_func(self):
        found = resolve('/history/')
        self.assertEqual(found.func, history)

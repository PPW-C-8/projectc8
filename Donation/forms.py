from django import forms

class FormDonasi(forms.Form):
    program = forms.CharField(label='Program', max_length=50, required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter the Program'}))
    jumlah_donasi = forms.IntegerField(label='Jumlah Donasi', required=True,
                                       widget=forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'How Much?'}))
    anonim = forms.BooleanField(required=False, initial=False)

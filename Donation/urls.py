from django.urls import path
from .views import *

urlpatterns = [
    path('', brosur, name='donasi'),
    path('donasi/', brosur, name='Submission'),
    path('history/', history, name='history')
]

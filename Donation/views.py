from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import *
from Program.models import *
from .forms import *


def Submission(request):
    all_donasi = donasi.objects.all()
    form = FormDonasi()
    html = "Submission.html"
    return render(request, html, {'donasi': all_donasi, 'form': form})

@login_required
def brosur(request):
    all_donasi = donasi.objects.all()
    if (request.method == "POST"):
        form = FormDonasi(request.POST or None)
        if form.is_valid():
            user = request.user
            program = request.POST["program"]
            jumlah_donasi = request.POST["jumlah_donasi"]
            anonim = request.POST.get("anonim", False)
            if Program.objects.filter(kodeProgram=program).exists():
                post = donasi(program=program, username=user.username, email=user.email, jumlah_donasi=jumlah_donasi, anonim=anonim)
                post.save()
                return HttpResponseRedirect('/history/')
            else:
                return HttpResponseRedirect('/donasi/')
    else:         
        form = FormDonasi()
    return render(request, 'Submission.html', {'donasi': all_donasi, 'form': form})

def history(request):
    user = request.user
    riwayat = donasi.objects.filter(username=user.username)
    jumlah = 0
    for sum in riwayat:
        jumlah += sum.jumlah_donasi

    return render(request, 'riwayat.html', {'riwayat': riwayat, 'jumlah': jumlah})



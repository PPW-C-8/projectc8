from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest


class HomePageTest(TestCase):
    def test_url_exist(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_main_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'Home.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage_view)

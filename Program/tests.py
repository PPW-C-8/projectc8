from django.test import TestCase, Client
from .models import Program


# Create your tests here.
from django.urls import resolve

from Program.views import *


class AppTest(TestCase):

    def test_view_fucntion(self):
        Program.objects.create(kodeProgram='program-1', judul='test', nama='test', tanggal='21 Oktober 2018',
                               link = 'test', deskripsi='test', kodeDonasi='default')
        self.assertTemplateNotUsed(self.client.get('/program-1/'), 'Donasi/templates/Submission.html')

    def test_a(self):
        Program.objects.create(kodeProgram='program-1', judul='test', nama='test', tanggal='21 Oktober 2018',
                               link='test', deskripsi='test', kodeDonasi='test')
        self.assertTemplateNotUsed(self.client.get('/program-1/'), 'templates/baseProgram.html')

    def test_calculate(self):
        donasi.objects.create(program="prog1", username="as", email="asd@asd.com", jumlah_donasi=100000, anonim=False)
        self.assertEqual(calculateAll("prog1"), "Donasi yang Telah Terkumpul Sebesar Rp. 100000")

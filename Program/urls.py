from django.urls import path
from .views import *

urlpatterns = [
    path('program-1/', program, name='program-1'),
    path('program-2/', program, name='program-2'),
    path('program-3/', program, name='program-3'),
    path('program-4/', program, name='program-4'),
    path('program-5/', program, name='program-5'),
    path('program-6/', program, name='program-6'),
    path('program-7/', program, name='program-7'),
    path('program-8/', program, name='program-8'),
    path('program-9/', program, name='program-9'),
    path('program-10/', program, name='program-10'),
]

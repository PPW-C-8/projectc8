from django.db import models

# Create your models here.
class Program(models.Model):
    kodeProgram = models.CharField(max_length=50)
    judul = models.CharField(max_length=50)
    nama = models.CharField(max_length=100)
    tanggal = models.CharField(max_length=100)
    link = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=500)
    kodeDonasi = models.CharField(max_length=500)


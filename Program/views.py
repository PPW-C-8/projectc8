from django.shortcuts import render, redirect
from django.urls import resolve
from Donation.models import donasi
from .models import Program

response = {}
def program(request):
    response['kode'] = 'default'
    url = resolve(request.path).url_name
    for program in Program.objects.all():
        if (program.kodeProgram == url):
            response["judul"] = program.judul
            response["nama"] = program.nama
            response["tanggal"] = program.tanggal
            response["deskripsi"] = program.deskripsi
            response["link"] = program.link
            response['kode'] = program.kodeDonasi
            response["donasi"] = calculateAll(response["kode"])
            break
    if (response["kode"] == "default"):
        return redirect('donasi')
    return render(request, 'baseProgram.html', response)

def calculateAll(type):
    uang = 0
    for donation in donasi.objects.all():
        if (donation.program == type):
            uang += donation.jumlah_donasi
    if (uang>0):
        return "Donasi yang Telah Terkumpul Sebesar Rp. {}".format(uang)
    return "Belum ada donasi. Jadilah yang pertama!"
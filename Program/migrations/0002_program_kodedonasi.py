# Generated by Django 2.1.1 on 2018-10-19 02:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Program', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='kodeDonasi',
            field=models.CharField(default='test', max_length=500),
            preserve_default=False,
        ),
    ]

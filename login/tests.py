from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class LoginUnitTest(TestCase):

    def test_login_url_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exists(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 200)

    def test_login_func(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login_view)

    def test_logout_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout_view)

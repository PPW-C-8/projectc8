from django.urls import path
from .views import *

urlpatterns = [
    path(r'login/', login_view, name='login'),
    path(r'logout/', logout_view, name='logout'),
]

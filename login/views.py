from django.shortcuts import render
from django.contrib.auth import logout as logout_user

def login_view(request):
    return render(request, "Login.html")

def logout_view(request):
    logout_user(request)
    return render(request, "Login.html")
